#pragma once

#include "ListenElement.h"

using namespace std;

class Liste
{
public:
	void Add(const int value);
	bool Get(const int index, int* pResult) const;
	void Delete(const int index);

private:
	shared_ptr<ListenElement> m_pHead;
};