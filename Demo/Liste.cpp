#include "Liste.h"
#include <future>

void Liste::Add(const int value)
{
	shared_ptr<ListenElement> pElement = make_shared<ListenElement>(value);

	if (!m_pHead)
	{
		m_pHead = pElement;
	}
	else
	{
		shared_ptr<ListenElement> pCurrent = m_pHead;
		while (pCurrent->HasNext())
		{
			pCurrent = pCurrent->GetNext();
		}

		pCurrent->SetNext(pElement);
	}
}

bool Liste::Get(const int index, int* pResult) const
{
	if(!m_pHead || !pResult || index < 0)
	{
		return false;
	}

	if(index == 0)
	{
		*pResult = m_pHead->GetValue();
		return true;
	}

	int currentIndex = 1;
	shared_ptr<ListenElement> pCurrent = m_pHead->GetNext();
	while(pCurrent && currentIndex != index)
	{
		pCurrent = pCurrent->GetNext();
		currentIndex++;
	}

	if(pCurrent && currentIndex == index)
	{
		*pResult = pCurrent->GetValue();
		return true;
	}

	return false;
}

void Liste::Delete(const int index)
{
	if(!m_pHead || index < 0)
	{
		return;
	}

	if(index == 0)
	{
		m_pHead = m_pHead->GetNext();
		return;
	}

	int currentIndex = 1;
	shared_ptr<ListenElement> pPrev = m_pHead;
	shared_ptr<ListenElement> pCurrent = m_pHead->GetNext();

	while(pCurrent && currentIndex != index)
	{
		pPrev = pCurrent;
		pCurrent = pCurrent->GetNext();
		currentIndex++;
	}

	if(pCurrent && currentIndex == index)
	{
		pPrev->SetNext(pCurrent->GetNext());
	}
}
