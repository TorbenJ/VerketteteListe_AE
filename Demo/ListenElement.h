#pragma once

#include <memory>

using namespace std;

class ListenElement
{
public:
	ListenElement(const int value);

	int GetValue() const;
	void SetValue(const int value);
	bool HasNext() const;
	shared_ptr<ListenElement> GetNext() const;
	void SetNext(const shared_ptr<ListenElement> pNext);

private:
	int m_value;
	shared_ptr<ListenElement> m_pNext;
};