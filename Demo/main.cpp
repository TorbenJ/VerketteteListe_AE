#include "Liste.h"

int main()
{
	Liste liste;

	liste.Add(1);
	liste.Add(2);
	liste.Add(3);

	int i1, i2, i3;
	bool success = liste.Get(0, &i1);
	success = liste.Get(1, &i2);
	success = liste.Get(2, &i3);
	success = liste.Get(3, &i1);

	liste.Delete(3);
	liste.Delete(0);
	liste.Delete(1);
	liste.Delete(0);
	liste.Delete(0);

	return 0;
}