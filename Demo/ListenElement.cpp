#include "ListenElement.h"

ListenElement::ListenElement(const int value)
	: m_value(value)
{
}

int ListenElement::GetValue() const
{
	return m_value;
}

void ListenElement::SetValue(const int value)
{
	m_value = value;
}

bool ListenElement::HasNext() const
{
	return m_pNext.get() != nullptr;
}

shared_ptr<ListenElement> ListenElement::GetNext() const
{
	return m_pNext;
}

void ListenElement::SetNext(const shared_ptr<ListenElement> pNext)
{
	m_pNext = pNext;
}
